
import java.io.File;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.LinkedList;

import java.util.List;
import java.util.ListIterator;
import java.util.Collections;
import java.util.Comparator;

import java.time.LocalDate;
import java.time.LocalTime;


public class Mapper {
    Scan x = new Scan();

    public void sortWords(Map<String,Integer> mapsPassedIn){

        HashMap<String, Integer> map = mapsPassedIn;

           // HashMap<String, Integer> map = x.getWordMap();
            List<Entry<String, Integer>> sortedList = new LinkedList<Entry<String, Integer>>(map.entrySet());
            //Sorting by value.
            Collections.sort(sortedList, new Comparator<Entry<String, Integer>>() {

                /**
                 * Going to check values.
                 * @param o1
                 * @param o2
                 * @return
                 */
                public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
                    return o1.getValue().compareTo(o2.getValue());
                }
            });
            ListIterator i = sortedList.listIterator(sortedList.size());

            /**
             * Printing out the findings.
             * Both the most seen word and the less seen word.
             */

            System.out.println("Woaah, there are " + sortedList.size() + " different words in this text file. ");
            System.out.println("***********************************************");
            System.out.println("Here are the top Five that appear the most throught the file:");

            //setting up the local time: for the new filename.
        LocalDate localDataObj = LocalDate.now();
        LocalTime localTimeObj = LocalTime.now();

        String fileNameWithAnswers = "WordCountFor"+ localDataObj + localTimeObj;
        PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
            while (i.previousIndex() != sortedList.size() - 6) {
                Object e = i.previous();
                System.out.println(e);
            }
            System.out.println("***********************************************");
            System.out.println("Here are the words that apear the least in this test file: ");

            int countForFirstLetters = 0; //Int counter to get the first words.
            while (countForFirstLetters <= 4) {
                System.out.println(sortedList.get(countForFirstLetters));
                countForFirstLetters++;
            }

            return
            System.out.println("***********************************************");
            System.out.println("DONE!!!!! ");
        }

    }
