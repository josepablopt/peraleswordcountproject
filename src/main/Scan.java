import java.io.File;
import java.io.PrintWriter;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.LinkedList;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Collections;
import java.util.Comparator;

import java.time.LocalDate;
import java.time.LocalTime;



/**
 * Public class scan.
 */

public class Scan {

    /**
     * this is a  function that calls a scanner and
     * also creates a linked list sorting everything into a good position.
     */

    public static void scanner2(String fileNamePassedIn, String fileNameToDownload, String fileNameToRemove) {
        /**
         * Using a hashmap to store the words that we find.
         */
        Map<String, Integer> map = new HashMap<String, Integer>();

        /**
         * Creating a scanner for user inoput for their file.
         * Creating a file into the file to pass it in.
         */

        File file = new File(fileNamePassedIn);
        if (fileNameToDownload != "") {
            System.out.println("File Name that was given: " + fileNameToDownload);
        } else {
            System.out.println("There was no File Name given :(");
        }


        /**
         * Going into a Try Catch statement. If it can't find it, its going to throw a FileNotFoundException.
         * Have not done a lot of catch.
         */

        try (Scanner txtFile = new Scanner(file)) {
            while (txtFile.hasNext()) {
                String word = txtFile.next();


                /**
                 * Doing some cleaning up of the words to avoid a word being counted as double. For Example,
                 * Perales and Perales. are the same word but here we're checking to see if there is a
                 * . , ? ! ; : at the end and removing it so its placed in the same key in the HashMap.
                 *
                 */
                word.toLowerCase();
                if (word.contains(".")) {
                    word = word.substring(0, word.length() - 1);
                } else if (word.contains(",")) {
                    word = word.substring(0, word.length() - 1);
                } else if (word.contains(":")) {
                    word = word.substring(0, word.length() - 1);
                } else if (word.contains("?")) {
                    word = word.substring(0, word.length() - 1);
                } else if (word.contains(";")) {
                    word = word.substring(0, word.length() - 1);
                } else if (word.contains("!")) {
                    word = word.substring(0, word.length() - 1);
                }

                /**
                 * Checking to see if the word is already in the HashMap, add one to it
                 * else add the new word to the HashMap.
                 */

                if (map.containsKey(word)) {
                    int count = map.get(word) + 1;
                    map.put(word, count);
                } else {
                    map.put(word, 1);
                }

            } //End the while loop and close the scanner in the next line.
            txtFile.close();

            /**
             * Moving on to the Sorthing Checker!!!!
             */
//            sortWordsIntoPositions.sortWords();

        } catch (FileNotFoundException exp) {
            exp.printStackTrace();
        }


        List<Entry<String, Integer>> sortedList = new LinkedList<Entry<String, Integer>>(map.entrySet());
        //Sorting by value.
        Collections.sort(sortedList, new Comparator<Entry<String, Integer>>() {

            /**
             * Going to check values.
             * @param o1
             * @param o2
             * @return
             */
            public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        ListIterator i = sortedList.listIterator(sortedList.size());

        /**
         * Printing out the findings.
         * Both the most seen word and the less seen word.
         */

        //setting up the local time: for the new filename.
        LocalDate localDataObj = LocalDate.now();
        LocalTime localTimeObj = LocalTime.now();
        String fileNameWithAnswers = "WordCountFor" + localDataObj + localTimeObj+".txt";
        System.out.println(fileNameWithAnswers);
        System.out.println(fileNameToDownload);

        String master = fileNamePassedIn;
        String target = fileNameToRemove;
        String replacement = fileNameToDownload;
        String processed = master.replace(target, replacement);

        File fileToBeDownloaded = new File(processed);
        fileToBeDownloaded.getParentFile().mkdirs();

        try {
            //Creating the file
            PrintWriter writer = new PrintWriter(processed);
            //First Lines in the Document
            writer.println("Welcome. Word Count Project Created by Jose Perales has found the top 5 word that occur the most and least frequently from the following file: " + file);
            writer.println("");
            writer.println("");
            writer.println("Woaah, there are " + sortedList.size() + " different words in this text file. ");
            writer.println("");
            writer.println("");
            writer.println("***********************************************");
            writer.println("Here are the top five words");
            while (i.previousIndex() != sortedList.size() - 6) {
                Object e = i.previous();
                writer.println(e);
                System.out.println(e);
//            wordCountTop5.addLast(e);
            }

            int countForFirstLetters = 0; //Int counter to get the first words.
            writer.println("");
            writer.println("***********************************************");
            writer.println("");
            writer.println("Here are the bottom five words");
            while (countForFirstLetters <= 4) {
                System.out.println(sortedList.get(countForFirstLetters));
                writer.println(sortedList.get(countForFirstLetters));
                //   wordCountBottom5.add(sortedList.get(countForFirstLetters));
                countForFirstLetters++;

            }

            writer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("WOWOWOWOWOWOWOWOWOWOWOWOW THERE IS AN ERROR:: ");

        }

        //Check to see if its done. if is, return the data!!!


    }



}




