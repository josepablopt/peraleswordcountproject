/**
 * Jose Perles
 * /src/main/interface.java
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Interface extends JFrame implements ActionListener {


    //  private JButton convertButton = new JButton("File");
    //  private JTextField testTemp = JTextField(20);
    // private JLabel testLable = JLabel("test");
    private JLabel filePathAnswer = new JLabel("No File Selected");
    private JButton fileButton = new JButton("Which File?");
    private JLabel newPrintFileName = new JLabel("File Name");
    private JLabel instructionstxt = new JLabel(".Txt");
    private JLabel fileLocationPathLabel = new JLabel("File Path:");
    private JTextField newFileNameToPrint = new JTextField(20);
    private JLabel answes = new JLabel("waiting");

    private JLabel IsDoneSetting = new JLabel("  ");




    public Interface() {
        //The whole windown
        this.setTitle("Perales Word Count Project!");
        this.setBounds(500, 600, 850, 450);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //this.convertButton.setBounds(10, 80, 100, 30);
        // this.getContentPane().add(convertButton);
        //   this.convertButton.addActionListener(this);

        //this is the button.
        this.fileButton.setBounds(325, 375, 100, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener(this);


        //Print new File Name label and the .
        this.newPrintFileName.setBounds(10, 20, 100, 30);
        this.getContentPane().add(newPrintFileName);
        this.instructionstxt.setBounds(225, 23, 100, 30);
        this.getContentPane().add(instructionstxt);
        this.newFileNameToPrint.setBounds(120, 20, 100, 30);
        this.getContentPane().add(newFileNameToPrint);

        //File Path label and location
        this.fileLocationPathLabel.setBounds(10, 60, 100, 30);
        this.getContentPane().add(fileLocationPathLabel);
        this.filePathAnswer.setBounds(120, 60, 700, 30);
        this.getContentPane().add(filePathAnswer);

        //Print new File Name label and the .
        this.IsDoneSetting.setBounds(10, 90, 700, 30);
        this.getContentPane().add(IsDoneSetting);
//        this.newFileNameToPrint.setBounds(120, 20, 100, 30);
//        this.getContentPane().add(newFileNameToPrint);


    }

    private void theFileButtonHasBeenPushed() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        int chooserSuccess = chooser.showOpenDialog(null);

        if (chooserSuccess == JFileChooser.APPROVE_OPTION) {
            Scan fileScanner = new Scan();
            String newFileNameFromUser = newFileNameToPrint.getText();
            if(newFileNameToPrint.getText() == "" || newFileNameToPrint.getText() == null ){
                newFileNameFromUser = "noNameFile.txt";
            }
            File chosenFile = chooser.getSelectedFile();
            String FileLocation = chosenFile.getAbsolutePath();
            //chosenFile will get passed to the funtion!!!!
            this.filePathAnswer.setText(chosenFile.getAbsolutePath());
            fileScanner.scanner2(chosenFile.getAbsolutePath(), newFileNameFromUser, chosenFile.getName());
            System.out.println("The New filename that was given is:" + newFileNameFromUser);
            System.out.println("You chose the file " + chosenFile.getAbsolutePath());
            System.out.println("You chose the file " + chosenFile.getName());
            isDone(newFileNameFromUser);

        } else {
            System.out.println("Smh, you hit cancel line 53");
        }


    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("The action event is" + e);
        if (e.getActionCommand().equals("Which File?")) {
            this.theFileButtonHasBeenPushed();
        }
    }

    private void isDone(String newFileNameFromUser){
        String answer = "Your new file is located is the same folder with the file name: "+newFileNameFromUser + ".txt";
        this.IsDoneSetting.setText(answer);
    }


}